import org.junit.Assert;
import org.junit.Test;


public class PairedNTests {

	@Test
	public void shouldPass1() {
		
		int expected = 1;
		int actual = PairedN.isPairedN( new int[] {1, 4, 1, 4, 5, 6}, 5 );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		
		int expected = 1;
		int actual = PairedN.isPairedN( new int[] {1, 4, 1, 4, 5, 6}, 6);
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass3() {
		
		int expected = 1;
		int actual = PairedN.isPairedN( new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8}, 6);
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass4() {
		
		int expected = 0;
		int actual = PairedN.isPairedN( new int[] {1, 4, 1}, 5);
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass5() {
		
		int expected = 0;
		int actual = PairedN.isPairedN( new int[] {8, 8, 8, 8, 7, 7, 7}, 15);
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass6() {
		
		int expected = 0;
		int actual = PairedN.isPairedN( new int[] {8, -8, 8, 8, 7, 7, -7}, -15);
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass7() {
		
		int expected = 0;
		int actual = PairedN.isPairedN( new int[] {3}, 3);
		Assert.assertEquals(expected, actual);

	}

}
