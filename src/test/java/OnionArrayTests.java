import org.junit.Assert;
import org.junit.Test;

public class OnionArrayTests {

	@Test
	public void shouldPass1() {
		int expected = 1;
		int actual = OnionArray.isOnionArray( new int[] {1, 2, 19, 4, 5} );
		Assert.assertEquals(expected, actual);
	}

	
	@Test
	public void shouldPass2() {
		int expected = 0;
		int actual = OnionArray.isOnionArray( new int[] {1, 2, 3, 4, 15} );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass3() {
		int expected = 0;
		int actual = OnionArray.isOnionArray( new int[] {1, 3, 9, 8} );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass4() {
		int expected = 1;
		int actual = OnionArray.isOnionArray( new int[] {2} );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass5() {
		int expected = 1;
		int actual = OnionArray.isOnionArray( new int[] {} );
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void shouldPass6() {
		int expected = 1;
		int actual = OnionArray.isOnionArray( new int[] {-2, 5, 0, 5, 12} );
		Assert.assertEquals(expected, actual);
	}

}
