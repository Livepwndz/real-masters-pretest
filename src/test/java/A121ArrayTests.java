import org.junit.Assert;
import org.junit.Test;


public class A121ArrayTests {

	@Test
	public void shouldPass1() {
		int expected = 1;
		int actual = A121Array.is121Array ( new int[] {1, 2, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass2() {
		int expected = 1;
		int actual = A121Array.is121Array ( new int[] {1, 1, 2, 2, 2, 1, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass3() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {1, 1, 2, 2, 2, 1, 1, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass4() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {1, 1, 2, 1, 2, 1, 1}	 );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass5() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {1, 1, 1, 2, 2, 2, 1, 1, 1, 3} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass6() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {1, 1, 1, 1, 1, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass7() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass8() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {1, 1, 1, 2, 2, 2, 1, 1, 2, 2} );
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass9() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {2, 2, 2});
		Assert.assertEquals(expected, actual);

	}
	
	@Test
	public void shouldPass10() {
		int expected = 0;
		int actual = A121Array.is121Array ( new int[] {2, 2, 2,2});
		Assert.assertEquals(expected, actual);

	}

}
