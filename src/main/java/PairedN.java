
public class PairedN {

	public static int isPairedN(int[] a, int n) {

		if (a == null || a.length == 0) {
			return 0;
		}

		for (int i = 0; i < a.length; i++) {
			int value1 = a[i];
			for (int j = 0; j < a.length; j++) {
				if (i == j) {
					continue;
				}

				int value2 = a[j];
				if (value1 != value2) {
					int sumOfValues = value1 + value2;
					int sumOfIndex = i + j;
					if (sumOfValues == n && sumOfIndex == n) {
						return 1;
					}
				}
			}

		}
		return 0;
	}

}
