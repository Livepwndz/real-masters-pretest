
public class OnionArray {

	public static int isOnionArray(int[] a) {
		if (a == null) {
			return 0;
		}

		if (a.length == 0) {
			return 1;
		}

		for (int i = 0; i < a.length; i++) {

			int j = i;
			int k = a.length - i - 1;

			if (j != k) {
				int sum = a[j] + a[k];
				if (!(sum <= 10)) {
					return 0;
				}
			}

		}

		return 1;
	}

}
