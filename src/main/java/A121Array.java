
public class A121Array {

	public static int is121Array(int[] a) {

		if (a == null || a.length < 3) {
			return 0;
		}

		int firstValue = a[0];
		int lastValue = a[a.length - 1];

		if (firstValue != 1 || lastValue != 1) {
			return 0;
		}

		int countOfFirstOnes = 0;
		int countOfLastOnes = 0;

		boolean isFirstOnesCountComplete = false;
		boolean isTwosCountComplete = false;

		for (int i = 0; i < a.length; i++) {
			int value = a[i];

			if (!(value == 1 || value == 2)) {
				return 0;
			}

			if (!isFirstOnesCountComplete && value == 1) {
				countOfFirstOnes++;
			} else if (!isTwosCountComplete && value == 2) {
				isFirstOnesCountComplete = true;
			}

			if (isFirstOnesCountComplete && value == 1) {
				isTwosCountComplete = true;
				countOfLastOnes++;
			}

			if (isTwosCountComplete && value == 2) {
				return 0;
			}
		}

		if (countOfFirstOnes != countOfLastOnes) {
			return 0;
		}

		return 1;
	}

}
